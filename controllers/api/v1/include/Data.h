#pragma once

#include <drogon/HttpController.h>

namespace api::v1 {
    class Data : public drogon::HttpController<Data> {
    public:
        METHOD_LIST_BEGIN
            METHOD_ADD(Data::post, "", drogon::Post);
            METHOD_ADD(Data::post, "/", drogon::Post);
        METHOD_LIST_END

        static drogon::Task<> post(drogon::HttpRequestPtr req, std::function<void (const drogon::HttpResponsePtr &)> callback);
    };
}
