#include "Data.h"
#include "utils.h"

#include <cryptopp/aes.h>
#include <cryptopp/gcm.h>
#include <cryptopp/filters.h>


static inline auto insertSensorData(const Json::Value &jsonRequest)-> drogon::Task<> {
    auto soilTemperature{jsonRequest["st"].as<double>()};
    auto soilMoisture{jsonRequest["sm"].as<double>()};
    auto airTemperature{jsonRequest["at"].as<double>()};
    auto humidity{jsonRequest["h"].as<double>()};

    co_await drogon::app().getDbClient()->execSqlCoro(R"*(
        INSERT INTO sensors_log(soil_temperature, soil_moisture, air_temperature, humidity)
        VALUES ((CASE ($1)::FLOAT WHEN 0 THEN NULL ELSE $1 END),
                (CASE ($2)::FLOAT WHEN 0 THEN NULL ELSE $2 END),
                (CASE ($3)::FLOAT WHEN 0 THEN NULL ELSE $3 END),
                (CASE ($4)::FLOAT WHEN 0 THEN NULL ELSE $4 END));)*",
                soilTemperature, soilMoisture, airTemperature, humidity);
    co_return;
}

drogon::Task<>
api::v1::Data::post(drogon::HttpRequestPtr req, std::function<void(const drogon::HttpResponsePtr &)> callback) {
    if (req->jsonObject() == nullptr) {
        Json::Value jsonResponse;
        jsonResponse["success"] = false;
        jsonResponse["error"] = "Content-Type: application/json or json data is missing";
        auto httpResponse = drogon::HttpResponse::newHttpJsonResponse(jsonResponse);
        httpResponse->setStatusCode(drogon::k400BadRequest);
        callback(httpResponse);
        co_return;
    }

    auto jsonReq{*req->getJsonObject()};

    if (!jsonReq.isMember("iv")) {
        callback(getInvalidJsonItemRes("iv"));
        co_return;
    } else if (!jsonReq.isMember("tagSize")) {
        callback(getInvalidJsonItemRes("tagSize"));
        co_return;
    } else if (!jsonReq.isMember("cipher")) {
        callback(getInvalidJsonItemRes("cipher"));
        co_return;
    }

    auto ivString{drogon::utils::base64Decode(jsonReq["iv"].as<std::string>())};
    auto tagSize{jsonReq["tagSize"].as<int>()};
    auto cipherDataString{drogon::utils::base64Decode(jsonReq["cipher"].as<std::string>())};
    std::string data;

    CryptoPP::SecByteBlock key(ENC_PASSWD, sizeof(ENC_PASSWD));
    CryptoPP::SecByteBlock iv(reinterpret_cast<unsigned char *>(ivString.data()), ivString.size());

    try {
        CryptoPP::GCM<CryptoPP::AES>::Decryption decryption;
        decryption.SetKeyWithIV(key, key.size(), iv, iv.size());
        CryptoPP::AuthenticatedDecryptionFilter decryptionFilter(decryption, new CryptoPP::StringSink(data),
                                                                 CryptoPP::AuthenticatedDecryptionFilter::Flags::DEFAULT_FLAGS,
                                                                 tagSize);
        { CryptoPP::StringSource ss(cipherDataString, true, new CryptoPP::Redirector(decryptionFilter)); }
        if (decryptionFilter.GetLastResult()) {
            co_await insertSensorData(jsonDecode(data));
            Json::Value jsonResponse;
            jsonResponse["success"] = true;
            auto httpResponse = drogon::HttpResponse::newHttpJsonResponse(jsonResponse);
            httpResponse->setStatusCode(drogon::k200OK);
            callback(httpResponse);
        } else {
            Json::Value jsonResponse;
            jsonResponse["success"] = false;
            jsonResponse["error"] = "Decrypted message is not authenticated";
            auto httpResponse = drogon::HttpResponse::newHttpJsonResponse(jsonResponse);
            httpResponse->setStatusCode(drogon::k401Unauthorized);
            callback(httpResponse);
        }
    } catch (const CryptoPP::Exception &exception) {
        Json::Value jsonResponse;
        jsonResponse["success"] = false;
        jsonResponse["error"] = "Failed to decrypt the sent payload: (" + exception.GetWhat() + ")";
        auto httpResponse = drogon::HttpResponse::newHttpJsonResponse(jsonResponse);
        httpResponse->setStatusCode(drogon::k401Unauthorized);
        callback(httpResponse);
    }
}