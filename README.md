# Capstone Server

This is a server for the capstone project which provides an endpoint `/api/v1/data` to which the esp8266 should send
sensor data in `json`

## Requirements

- [drogon](https://github.com/drogonframework/drogon)
- [cryptopp](https://github.com/weidai11/cryptopp)

## Installation

```bash
git clone https://gitlab.com/iot187/apps/stem-capstone/capstone-server ~/projects/capstone-server
cd capstone-server
cmake -B cmake-build-release -S . -DCMAKE_BUILD_TYPE=Release
cmake --build cmake-build-release --parallel 8 --target capstone-server
./cmake-build-release/capstone-server -c config.json
```

## Usage

The server is meant to run with the esp8266 firmware where it receives data sent to its endpoint `/api/v1/data` and save
it a database. The database in turn is used by grafana to plot the graph.

The data is sent in the following format:

```json
{
  "st": 22.125,
  "sm": 10.2367,
  "at": 0.0,
  "h": 0.0
}
```

where `st` is soil temperature, `sm` is soil moisture, `at` is air temperature, `h` is the humidity. Values equal to 0
are stored as NULL in the database since temperature is intended to be stored in kelvins otherwise the SQL logic should
be changed to match the required use case.

## License

[GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/)
