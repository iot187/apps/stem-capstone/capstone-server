#include <drogon/drogon.h>

#include "utils.h"


int main(int argc, char **argv) {
    auto programOptions = parseArgs(argc, argv);

    if (programOptions.find(MAP_CONFIG_FILE) == programOptions.end()) {
        LOG_ERROR << "No config file was provided\n" << argv[0] << " -c <config-file>";
        exit(1);
    }

    drogon::app().loadConfigFile(programOptions[MAP_CONFIG_FILE]);
    drogon::app().run();
    return EXIT_SUCCESS;
}