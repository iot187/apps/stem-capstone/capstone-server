#pragma once

#include <json/json.h>
#include <drogon/drogon.h>
#include <getopt.h>

#include "constants.h"

#define MAP_CONFIG_FILE "configFile"


/**
 * Return a response indicating the provided item doesn't exist in the request json
 * @param item Item missing from the json object
 * @return HttpResponsePtr to be returned as response
 */
inline auto getInvalidJsonItemRes(const std::string &item) {
    Json::Value jsonResponse;
    jsonResponse["success"] = false;
    jsonResponse["error"] = "missing field '" + item + "' in json request";

    auto httpResponse = drogon::HttpResponse::newHttpJsonResponse(jsonResponse);
    httpResponse->setStatusCode(drogon::k400BadRequest);
    return httpResponse;
}

/**
 * Converts json object to string
 * @param json Json object
 * @return json string
 */
inline auto jsonEncode(const Json::Value &json) {
    Json::StreamWriterBuilder writerBuilder;
    writerBuilder["indentation"] = "";
    return Json::writeString(writerBuilder, json);
}

/**
 * Converts a string to json object
 * @param string Json object as a string
 * @return json object
 */
inline auto jsonDecode(const std::string &string) {
    Json::CharReaderBuilder readerBuilder;
    std::unique_ptr<Json::CharReader> charReader(readerBuilder.newCharReader());
    Json::Value json;
    if (charReader->parse(string.c_str(), string.c_str() + string.size(), &json, nullptr))
        return json;
    throw std::invalid_argument("invalid json");
}

inline std::unordered_map<std::string, std::string> parseArgs(int argc, char **argv) {
    int opt;
    static struct option options[] = {{"config", required_argument, nullptr, 'c'}};
    std::unordered_map<std::string, std::string> programOptions;
    while ((opt = getopt_long(argc, argv, "c:", options, nullptr)) != -1) {
        switch (opt) {
            case 'c':
                programOptions[MAP_CONFIG_FILE] = optarg;
                break;
            default:
                LOG_ERROR << argv[optind - 1] << " is undefined";
                break;
        }
    }
    return programOptions;
}
